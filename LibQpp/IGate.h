#pragma once
#ifdef WINDOWS
#ifdef LIBQPP_EXPORTS
#define LIBQPP_API __declspec(dllexport)
#else
#define LIBQPP_API __declspec(dllimport)
#endif
#elif defined(LINUX)
#define LIBQPP_API 
#endif // _WIN32

class LIBQPP_API IGate
{
public:
	inline IGate(){}
	inline ~IGate(){}

	virtual void Apply(Qubit *qubit, size_t targetPosition, std::vector<size_t> conditions, size_t noOfEntangledQubits) throw (std::exception) {}
	virtual void Apply(Qubit *qubit, std::vector<size_t> affectedPositions, size_t noOfEntangledQubits) throw (std::exception){}
	

protected:

};

class LIBQPP_API XGate :public IGate
{
public:
	inline XGate()
	{

	}
	inline virtual ~XGate()
	{

	}
	void Apply(Qubit *qubit, std::vector<size_t> affectedPositions, size_t noOfEntangledQubits) throw (std::exception);

protected:
private:


};

class LIBQPP_API ZGate :public IGate
{
public:
	inline ZGate()
	{

	}
	inline virtual ~ZGate()
	{

	}
	void Apply(Qubit *qubit, std::vector<size_t> affectedPositions, size_t noOfEntangledQubits) throw (std::exception);
protected:
private:


};

class LIBQPP_API YGate :public IGate
{
public:
	inline YGate()
	{

	}
	inline virtual ~YGate()
	{

	}
	void Apply(Qubit *qubit, std::vector<size_t> affectedPositions, size_t noOfEntangledQubits) throw (std::exception);
protected:

private:


};


class LIBQPP_API HGate :public IGate
{
public:
	inline HGate()
	{
		
	}
	inline virtual ~HGate()
	{

	}
	void Apply(Qubit *qubit, std::vector<size_t> affectedPositions, size_t noOfEntangledQubits) throw (std::exception);
protected:
private:


};

class LIBQPP_API CNotGate :public IGate
{
public:
	inline CNotGate()
	{

	}
	inline virtual ~CNotGate()
	{

	}
	void Apply(Qubit *qubit, size_t targetPosition, std::vector<size_t> conditions, size_t noOfEntangledQubits) throw (std::exception);
protected:
private:


};

class LIBQPP_API CPhaseShiftGate :public IGate
{
public:
	inline CPhaseShiftGate()
	{
	}
	inline virtual ~CPhaseShiftGate()
	{

	}
	void Apply(Qubit *qubit, size_t targetPosition, std::vector<size_t> conditions, size_t noOfEntangledQubits) throw (std::exception);
protected:
private:


};