#include "stdafx.h"
#include "Qubit.h"
#include "OperationExceptions.h"
#include "QRegister.h"
#include "IGate.h"
#include "QRegisterOperations.h"


std::unique_ptr<QRegisterOperations>QRegisterOperations::instance_;
std::once_flag QRegisterOperations::gOnceFlag;

QRegisterOperations &QRegisterOperations::GetInstance()
{
#ifdef LINUX
	if(instance_==NULL){
		instance_=std::unique_ptr<QRegisterOperations>(new QRegisterOperations());
	}
#else
	std::call_once(gOnceFlag,
		[] {
		instance_.reset(new QRegisterOperations);
	});
#endif
	return *instance_.get();
}


void QRegisterOperations::FillWithZero(QRegister *quantumRegister)
{
	for (size_t i = 0; i < quantumRegister->size(); ++i){
		quantumRegister->push(QubitZero());
	}
}

void QRegisterOperations::CopyRegister(QRegister *target, QRegister *source,size_t startingPosition)
{
	size_t maxSize = source->size();
	assert(startingPosition < maxSize);
	for (size_t i = startingPosition; i < maxSize; i++) {
		target->push(source->at(i));
	}
	
}

void QRegisterOperations::FillWithPattern(QRegister *quantumRegister, const std::string bitString) throw (std::exception)
{
	if (bitString.size() > quantumRegister->size()){
		throw QRegisterOverflowException();
	}
	
	for (size_t i = 0; i < quantumRegister->size(); ++i){
		if (bitString.at(i)=='0'){
			quantumRegister->push(QubitZero());
		}
		else{
			quantumRegister->push(QubitOne());
		}
	}
}


