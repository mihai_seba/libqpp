// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#ifdef __unix__         
    #define LINUX
#elif defined(_WIN32) || defined(WIN32) 
    #define WINDOWS

#endif
#ifdef WINDOWS
    #include "targetver.h"
    #define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif // _WIN32



// Windows Header Files:
#include <memory>
#include <iostream>
#include <mutex>
#include <complex>
#include <algorithm>
#include <vector>
#include <exception>
#include <cmath>
#include <cstdlib>
#include <array>

#include <mkl.h>
#ifndef		EIGEN_USE_MKL_ALL
#define		EIGEN_USE_MKL_ALL
#endif
#include <Eigen/Dense>

