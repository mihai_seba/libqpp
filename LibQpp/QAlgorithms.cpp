// QAlgorithms.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Qubit.h"
#include "OperationExceptions.h"
#include "QRegister.h"
#include "IGate.h"
#include "GateFactory.h"
#include "QRegister.h"
#include "QAlgorithms.h"




QAlgorithms::QAlgorithms()
{
	gGateFactory = GateFactory();
}

QAlgorithms::QAlgorithms(std::shared_ptr<Oracle>obj){
	gOracleObj = obj;
}

QAlgorithms::~QAlgorithms()
{

}

Qubit QAlgorithms::GetResults()
{
	return gResultQubit;
}

void QAlgorithms::SetOracleMethod(void(Oracle::*method)(Qubit *qubit, size_t registerSize), size_t registerSize)
{
	gMethod = method;
	gRegisterSize = registerSize;
}

void QAlgorithms::ApplyOracle()
{
	(gOracleObj.get()->*gMethod)(&gResultQubit,gRegisterSize);
}


void QAlgorithms::SetOracleObj(std::shared_ptr<Oracle> obj)
{
	gOracleObj = obj;
}