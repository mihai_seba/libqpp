#include "stdafx.h"
#include "Qubit.h"

#ifdef WINDOWS
#ifdef LIBQPP_EXPORTS
#define LIBQPP_API __declspec(dllexport)
#else
#define LIBQPP_API __declspec(dllimport)
#endif
#elif defined(LINUX)
#define LIBQPP_API 
#endif // _WIN32

const double Qubit::Q_TOLERANCE = 1e-10;
Qubit::Qubit(std::complex<double> state0, std::complex<double> state1)
{
	//reserve space
	gQubitStateArray = Eigen::VectorXcd(2);
	gQubitStateArray(0) = state0;
	gQubitStateArray(1) = state1;
	gIsQubitMeasured = false;
	
}

Qubit::Qubit(Eigen::VectorXcd qubitStates)
{
	
	gQubitStateArray = qubitStates;
	gIsQubitMeasured = false;
}

Qubit::~Qubit()
{
	
}


Eigen::VectorXcd Qubit::GetQubitStates() const
{
	return gQubitStateArray;
}

std::string Qubit::to_string() const
{
	//print qubit states
	std::stringstream strStream;
	strStream << "[";
	for (size_t i = 0; i < gQubitStateArray.size(); i++){
		if (gQubitStateArray(i).imag() >= 0){
			strStream << gQubitStateArray(i).real() << "+" << gQubitStateArray(i).imag() << "i,";
		}
		else{
			strStream << gQubitStateArray(i).real() << gQubitStateArray(i).imag() << "i,";
		}
	}
	strStream << "]";
	return strStream.str();

}

bool Qubit::IsQubitMeasured() const
{
	return gIsQubitMeasured;
	
}

void Qubit::MeasureQubit()
{
	gIsQubitMeasured = true;
}

bool Qubit::IsValid()
{
	double sum = 0.0;
	double value = 0.0;
	double Q_TOLERANCE = 0.1e-20;
	for (size_t i = 0; i < gQubitStateArray.size(); i++){
		value = std::abs(gQubitStateArray(i));
		
		sum += value*value;
	}

	return (std::fabs(sum - 1.0)<Q_TOLERANCE);
}


bool Qubit::operator==(const Qubit& lhs) const
{
	for (size_t i = 0; i < gQubitStateArray.size(); ++i){
		if (gQubitStateArray(i) != lhs.GetQubitStates()(i))
		{
			return false;
		}
	}
	return true;
}

bool Qubit::operator!=(const Qubit& lhs) const
{
	return !operator==(lhs);
}





Qubit Qubit::Entangle(const Eigen::VectorXcd q1, const Eigen::VectorXcd q2)
{

	Eigen::Matrix<std::complex<double>, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> result;
	Eigen::RowVectorXcd q2Transpose = q2.transpose();
	result = q1*q2Transpose;

	return Qubit(Eigen::Map<Eigen::VectorXcd>(result.data(), q1.size()*q2.size()));

}