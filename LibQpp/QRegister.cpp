#include "stdafx.h"
#include "Qubit.h"
#include "OperationExceptions.h"
#include "QRegister.h"

QRegister::QRegister()
{
	gData = std::vector<Qubit>();
}
QRegister::QRegister(const size_t registerSize)
{
	gRegisterSize = registerSize;
	gData = std::vector<Qubit>();
	gData.reserve(gRegisterSize);
}


QRegister::~QRegister()
{
	gData.clear();
}

void QRegister::push(const Qubit q) throw (std::exception)
{
	if (gData.size() >= gRegisterSize){
		throw QRegisterOverflowException();
	}
	gData.push_back(q);
}

Qubit QRegister::pop()
{
	Qubit q = gData.back();
	gData.pop_back();
	return q;
}

void QRegister::reserve_space(const size_t registerSize)
{
	gRegisterSize = registerSize;
	gData.reserve(gRegisterSize);
}
Qubit &QRegister::operator[](const size_t idx)
{
	assert(idx >= 0);
	assert(idx < gRegisterSize);
	return gData[idx];
}
Qubit QRegister::at(const size_t idx)
{
	return gData.at(idx);
}
size_t QRegister::size() const
{
	return gRegisterSize;
}

size_t QRegister::length() const
{
	return gData.size();
}

bool QRegister::operator==(const QRegister& lhs) const
{
	if (gRegisterSize != lhs.size()){
		return false;
	}
	return (gData == lhs.gData);


}

bool QRegister::operator!=(const QRegister& lhs) const
{
	return !operator==(lhs);
}

Qubit QRegister::operator()(void)
{
	assert(gData.size() >= 2);
	if (gData.size() == 2) {
		return QubitZero();//TODO: Just kron between 2 qubits
	}

	size_t position = 0u;
	Qubit result = gData.at(0);
	for (size_t i = 0u; i < gData.size(); i++) {
		if (gData.at(i).IsQubitMeasured() == false) {
			result = gData.at(i);
			position = i + 1;
			break;
		}
	}
	for (size_t i = position; i < gData.size(); i++) {
		if (gData.at(i).IsQubitMeasured() == true) {
			continue;
		}
		result = result*gData.at(i);
	}
	return result;
}
