#pragma once
#ifdef WINDOWS
	#ifdef LIBQPP_EXPORTS
		#define LIBQPP_API __declspec(dllexport)
	#else
		#define LIBQPP_API __declspec(dllimport)
	#endif
#elif defined(LINUX)
	#define LIBQPP_API 
#endif // _WIN32

#include <iostream>
#include <exception>
class NullException:public std::exception
{
public:
	NullException(const std::string msg) :g_msg(msg)
	{
		
	}
	NullException() :g_msg("NullException"){}
	const char *what() const throw()
	{
		return g_msg.c_str();
	}
private:
	std::string g_msg;
};


class QubitOperationsException :public std::exception
{
public:
	QubitOperationsException(const std::string msg) :g_msg(msg)
	{

	}
	QubitOperationsException() :g_msg("Invalid lengths!"){}
	const char *what() const throw()
	{
		return g_msg.c_str();
	}
private:
	std::string g_msg;
};

class MatrixOperationsException :public std::exception
{
public:
	MatrixOperationsException(const std::string msg) :g_msg(msg)
	{

	}
	MatrixOperationsException() :g_msg("Invalid lengths!"){}
	const char *what() const throw()
	{
		return g_msg.c_str();
	}
private:
	std::string g_msg;
};

class QRegisterOverflowException :public std::exception
{
public:
	QRegisterOverflowException(const std::string msg) :g_msg(msg)
	{

	}
	QRegisterOverflowException() :g_msg("Quantum register overflow!"){}
	const char *what() const throw()
	{
		return g_msg.c_str();
	}
private:
	std::string g_msg;
};

class QBehavioralModelingException :public std::exception
{
public:
	QBehavioralModelingException(const std::string msg) :g_msg(msg)
	{

	}
	QBehavioralModelingException() :g_msg("Quantum register overflow!"){}
	const char *what() const throw()
	{
		return g_msg.c_str();
	}
private:
	std::string g_msg;
};