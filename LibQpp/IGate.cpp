#include "stdafx.h"
#include "Qubit.h"
#include "OperationExceptions.h"
#include "QRegister.h"
#include "IGate.h"







void XGate::Apply(Qubit *qubit, std::vector<size_t> affectedPositions, size_t noOfEntangledQubits) throw (std::exception)
{
	size_t mask = 0;
	size_t new_position = 0;
	Eigen::VectorXcd states = qubit->GetQubitStates();
	std::complex<double> bufferState;
	std::vector<size_t> marked_states(states.size());
	for (size_t i : affectedPositions){
		std::fill(marked_states.begin(), marked_states.end(), 0);
		mask = (1 << (noOfEntangledQubits - 1 - i));		
		for (size_t j = 0; j < states.size(); j++){
			if (marked_states[j] == 0){
				bufferState = states[j];
				new_position = j ^ mask;
				states[j] = states[new_position];
				states[new_position] = bufferState;
				marked_states[j] = 1;
				marked_states[new_position] = 1;				
			}
			continue;
			
		}
		mask = 0;
	}
	*qubit = Qubit(states);

}



void YGate::Apply(Qubit *qubit, std::vector<size_t> affectedPositions, size_t noOfEntangledQubits) throw (std::exception)
{

	size_t mask = 0;
	size_t new_position = 0;
	Eigen::VectorXcd states = qubit->GetQubitStates();
	std::vector<size_t> marked_states(states.size());
	std::complex<double>bufferState;
	for (size_t i : affectedPositions){
		std::fill(marked_states.begin(), marked_states.end(), 0);
		mask = (1 << (noOfEntangledQubits - 1 - i));
		for (size_t j = 0; j < states.size(); j++){
			if (marked_states[j] == 0){
				new_position = j ^ mask;
				bufferState = states[j];
				states[j] = std::complex<double>(0.0, -1.0)*states[new_position];
				states[new_position] = std::complex<double>(0.0, 1.0)*bufferState;
				marked_states[j] = 1;
				marked_states[new_position] = 1;
			}
			continue;

		}
		mask = 0;
	}
	*qubit = Qubit(states);
	


}



void CNotGate::Apply(Qubit *qubit, size_t targetPosition, std::vector<size_t> conditions, size_t noOfEntangledQubits) throw (std::exception)
{
	size_t mask = 0;
	size_t new_position = 0;
	Eigen::VectorXcd states = qubit->GetQubitStates();
	for (size_t i : conditions){
		mask |= (1 << (noOfEntangledQubits - 1 - i));
	}
	mask |= (1 << (noOfEntangledQubits - targetPosition));//useless
	new_position = mask | 0x01;
	std::complex<double> state = qubit->GetQubitStates()(mask);
	states[mask] = states[new_position];
	states[new_position] = state;


	*qubit = Qubit(states);
}
	





void HGate::Apply(Qubit *qubit, std::vector<size_t> affectedPositions,size_t noOfEntangledQubits) throw (std::exception)
{
	size_t mask = 0;
	size_t new_position = 0;
	double hCoefficient = 1.0 / std::sqrt(2);// std::pow(2, affectedPositions.size() / 2.0);
	Eigen::VectorXcd states = qubit->GetQubitStates();
	std::complex<double> bufferState;
	std::vector<size_t> marked_states(states.size());
	for (size_t i : affectedPositions){
		std::fill(marked_states.begin(), marked_states.end(), 0);
		mask = (1 << (noOfEntangledQubits - 1 - i));
		for (size_t j = 0; j < states.size(); j++){
			if (marked_states[j] == 0){
				new_position = j ^ mask;
				bufferState = states[j];
				states[j] = (bufferState + states[new_position])* hCoefficient;
				states[new_position]=(bufferState - states[new_position])* hCoefficient;
							
				marked_states[j] = 1;
				marked_states[new_position] = 1;
			}
			continue;

		}
		mask = 0;
	}
	*qubit = Qubit(states);
	

}



void ZGate::Apply(Qubit *qubit, std::vector<size_t> affectedPositions, size_t noOfEntangledQubits) throw (std::exception)
{
	size_t mask = 0;
	size_t new_position = 0;
	Eigen::VectorXcd states = qubit->GetQubitStates();
	std::vector<size_t> marked_states(states.size());
	for (size_t i : affectedPositions){
		std::fill(marked_states.begin(), marked_states.end(), 0);
		mask = (1 << (noOfEntangledQubits - 1 - i));
		for (size_t j = 0; j < states.size(); j++){
			if (marked_states[j] == 0){
				new_position = j ^ mask;
				states[j] *= std::complex<double>(1.0, 0.0);
				states[new_position] *= std::complex<double>(-1.0, 0.0);
				marked_states[j] = 1;
				marked_states[new_position] = 1;
			}
			continue;

		}
		mask = 0;
	}
	*qubit = Qubit(states);
	
}


void CPhaseShiftGate::Apply(Qubit *qubit, size_t targetPosition, std::vector<size_t> conditions, size_t noOfEntangledQubits) throw (std::exception)
{
	
	size_t mask = 0;
	size_t new_position = 0;
	Eigen::VectorXcd states = qubit->GetQubitStates();
	std::complex<double>bufferState;
	for (size_t i : conditions){
		mask |= (1 << (noOfEntangledQubits - 1 - i));
	}
	mask |= (1 << (noOfEntangledQubits - targetPosition-1));
	new_position = mask | 0x01;
	bufferState = states[mask];
	states[mask] *= std::complex<double>(-1.0,0.0);
	states[new_position] *= std::complex<double>(-1.0, 0.0);

	*qubit = Qubit(states);

}