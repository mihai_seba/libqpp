#include "stdafx.h"
#include "Qubit.h"
#include "OperationExceptions.h"
#include "QRegister.h"
#include "IGate.h"
#include "GateFactory.h"
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <random>
#include "MeasurementPerformer.h"

const double MeasurementPerformer::Q_TOLERANCE = 1e-10;
const size_t MeasurementPerformer::DEFAULT_NO_OF_TESTS = 100;
MeasurementPerformer::MeasurementPerformer()
{
	gNumberOfTests = DEFAULT_NO_OF_TESTS;

}
MeasurementPerformer::MeasurementPerformer(size_t numberOfTests)
{
	gNumberOfTests = numberOfTests;

}

MeasurementPerformer::~MeasurementPerformer()
{
}

void MeasurementPerformer::Configure(Qubit resultQubit)
{
	gResultQubit = resultQubit;

}

Qubit MeasurementPerformer::Measure()
{
	size_t position = IntervalCount();//FakeMeasure();
	CollapseState(position);
	return gResultQubit;
}

size_t MeasurementPerformer::IntervalCount()
{

	gLength = gResultQubit.GetQubitStates().size();
	Eigen::VectorXcd resultQubitStates = gResultQubit.GetQubitStates();
	std::vector<double>probabilities(gLength);
	std::vector<size_t> counterArray(gLength);
	std::mt19937 generator(time(NULL));
	std::uniform_real_distribution<double> distribution(0.0, 1.0);
	//std::srand(time(NULL));
	bool countTarget = false;
	double randomChoice = 0.0;
	std::vector<double>::iterator maxCount;
	//std::vector<size_t>::iterator maxCount;
	for (size_t i = 0; i < gLength; i++) {
		probabilities[i] = (resultQubitStates(i) * std::conj(resultQubitStates(i))).real();
		counterArray[i] = 0;
	}

	for (size_t count = 0; count < gNumberOfTests; count++) {
		randomChoice = distribution(generator);
		//randomChoice = std::rand();
		//randomChoice /= RAND_MAX;
		size_t i = 0;
		countTarget = true;
		while (probabilities[i] < randomChoice) {
			if (i < (gLength - 1)) {
				i++;
			}
			else
			{
				countTarget = false;
				break;
			}
		}
		if (countTarget == true) {
			counterArray[i]++;

		}

	}


	//Use for big number of qubits
	
	maxCount = std::max_element(probabilities.begin(), probabilities.end());
	return std::distance(probabilities.begin(), maxCount);
	/**/
	/*
	maxCount = std::max_element(counterArray.begin(), counterArray.end());
	return std::distance(counterArray.begin(), maxCount);
	*/
}



void MeasurementPerformer::CollapseState(size_t position)
{
	Eigen::VectorXcd collapsedQubit = gResultQubit.GetQubitStates();
	for (size_t i = 0; i < collapsedQubit.size(); i++) {
		collapsedQubit(i) = std::complex<double>(0.0, 0.0);
	}
	collapsedQubit(position) = std::complex<double>(1.0, 0.0);
	gResultQubit = Qubit(collapsedQubit);
}