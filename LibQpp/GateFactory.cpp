#include "stdafx.h"
#include "Qubit.h"
#include "OperationExceptions.h"
#include "QRegister.h"
#include "IGate.h"
#include "GateFactory.h"

GateFactory::GateFactory()
{

}

GateFactory::~GateFactory()
{

}
std::shared_ptr<IGate> GateFactory::CreateGate(enum EGateType gateType)
{

	std::shared_ptr<IGate> gate=nullptr;// (new HGate);
	switch (gateType)
	{
	case EHadamardGate:
		gate.reset(new HGate);
		break;
	case ECnotGate:
		gate.reset(new CNotGate);
		break;
	case EXGate:
		gate.reset(new XGate);
		break;
	case EZGate:
		gate.reset(new ZGate);
		break;
	case EYGate:
		gate.reset(new YGate());
		break;
	case ECPhaseShift:
		gate.reset(new CPhaseShiftGate);
		break;
	default:
		break;
	}

	return gate;
}