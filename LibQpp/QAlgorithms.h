#pragma once
#ifdef WINDOWS
	#ifdef LIBQPP_EXPORTS
		#define LIBQPP_API __declspec(dllexport)
	#else
		#define LIBQPP_API __declspec(dllimport)
	#endif
#elif defined(LINUX)
	#define LIBQPP_API 
#endif // _WIN32
class Oracle;
// This class is exported from the QAlgorithms.dll
class LIBQPP_API QAlgorithms {
public:
	QAlgorithms(void);
	QAlgorithms(std::shared_ptr<Oracle>obj);
	virtual ~QAlgorithms(void);
	inline void Init(){}
	inline void Run(){}
	inline void Measure(){}
	Qubit GetResults();
	
protected:
	void SetOracleObj(std::shared_ptr<Oracle> obj);
	void SetOracleMethod(void(Oracle::*method)(Qubit *qubit, size_t registerSize),size_t registerSize);
	void ApplyOracle();
	Qubit gResultQubit;
	GateFactory gGateFactory;
	QRegister gQregister;
private:
	std::shared_ptr<Oracle>gOracleObj;
	size_t gRegisterSize;
	void(Oracle::*gMethod)(Qubit *qubit,size_t registerSize);
};

