#include "stdafx.h"
#include "Qubit.h"
#include "QRegister.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(QRegisterTest)
	{
	public:
		
		TEST_METHOD(TestPush)
		{
			QRegister qRegister = QRegister(2);
			qRegister.push(QubitZero());
			qRegister.push(QubitOne());
			assert(qRegister[1] == QubitOne());
		}

		TEST_METHOD(TestPop)
		{
			QRegister qRegister = QRegister(2);
			qRegister.push(QubitZero());
			qRegister.push(QubitOne());
			assert(qRegister.pop() == QubitOne());
		}

		TEST_METHOD(TestSize)
		{
			QRegister qRegister = QRegister();
			qRegister.reserve_space(2);
			assert(qRegister.size() == 2);
		}
		TEST_METHOD(TestOperatorParantheses)
		{
			QRegister qRegister = QRegister(2);
			qRegister.push(QubitZero());
			qRegister.push(QubitOne());
			assert(qRegister[0] == QubitZero());
			qRegister[0] = QubitOne();
			assert(qRegister[1] == QubitOne());
		}

		TEST_METHOD(TestOperatorEqual)
		{
			QRegister reg1 = QRegister(2);
			reg1.push(QubitZero());
			reg1.push(QubitOne());
			QRegister reg2 = QRegister(2);
			reg2.push(QubitZero());
			reg2.push(QubitOne());
			assert(reg1 == reg2);
		}
		TEST_METHOD(TestOperatorNotEqual)
		{
			QRegister reg1 = QRegister(2);
			reg1.push(QubitZero());
			reg1.push(QubitZero());
			QRegister reg2 = QRegister(2);
			reg2.push(QubitOne());
			reg2.push(QubitOne());
			assert(reg1 != reg2);
		}

		TEST_METHOD(TestQRegisterIterators)
		{
			QRegister reg = QRegister(3);
			reg.push(QubitZero());
			reg.push(QubitZero());
			reg.push(QubitZero());
			assert(reg.size() == 3);
			for (auto elm : reg)
			{
				assert(static_cast<Qubit>(elm) == QubitZero());
	
			}
		}
	
	};
}