#include "stdafx.h"
#include "CppUnitTest.h"
#include "Qubit.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
BEGIN_TEST_MODULE_ATTRIBUTE()
TEST_MODULE_ATTRIBUTE(L"Date", L"2016/9/24")
END_TEST_MODULE_ATTRIBUTE()
namespace UnitTests
{
	TEST_CLASS(QubitTest)
	{
	public:

		TEST_METHOD(TestQubitConstructor)
		{
			Eigen::VectorXcd expectedStates(2);
			expectedStates(0) = std::complex<double>(1.0, 0.0);
			expectedStates(1) = std::complex<double>(0.0, 0.0);
			Qubit q = Qubit(std::complex<double>(1.0, 0.0), std::complex<double>(0.0, 0.0));
			assert(expectedStates(0) == q.GetQubitStates()(0));
			assert((expectedStates(1) == q.GetQubitStates()(1)));
		}

		TEST_METHOD(TestQubitZero)
		{
			Eigen::VectorXcd expectedStates(2);
			expectedStates(0) = std::complex<double>(1.0, 0.0);
			expectedStates(1) = std::complex<double>(0.0, 0.0);
			Qubit q = QubitZero();
			assert(expectedStates(0) == q.GetQubitStates()(0));
			assert((expectedStates(1) == q.GetQubitStates()(1)));
		}

		TEST_METHOD(TestQubitOne)
		{
			Eigen::VectorXcd expectedStates(2);
			expectedStates(0) = std::complex<double>(0.0, 0.0);
			expectedStates(1) = std::complex<double>(1.0, 0.0);
			Qubit q = QubitOne();
			assert(expectedStates(0) == q.GetQubitStates()(0));
			assert((expectedStates(1) == q.GetQubitStates()(1)));
		}

		TEST_METHOD(TestQubitPlus)
		{
			Eigen::VectorXcd expectedStates(2);
			expectedStates(0) = std::complex<double>(1.0/std::sqrt(2), 0.0);
			expectedStates(1) = std::complex<double>(1.0/ std::sqrt(2), 0.0);
			Qubit q = QubitPlus();
			assert(expectedStates(0) == q.GetQubitStates()(0));
			assert((expectedStates(1) == q.GetQubitStates()(1)));

		}

		TEST_METHOD(TestQubitMinus)
		{
			Eigen::VectorXcd expectedStates(2);
			expectedStates(0) = std::complex<double>(1.0 / std::sqrt(2), 0.0);
			expectedStates(1) = std::complex<double>(-1.0 / std::sqrt(2), 0.0);
			Qubit q = QubitMinus();
			assert(expectedStates(0) == q.GetQubitStates()(0));
			assert((expectedStates(1) == q.GetQubitStates()(1)));
		}

		TEST_METHOD(TestOperatorEqual)
		{
			Qubit q1 = QubitZero();
			Qubit q = QubitZero();
			assert(q1 == q);
		}

		TEST_METHOD(TestOperatorNotEqual)
		{
			Qubit q1 = QubitOne();
			Qubit q = QubitZero();
			assert(q1 != q);
		}

		TEST_METHOD(TestIsValid)
		{
			Qubit plus = QubitPlus();
			assert(plus.IsValid() == true);
		}

		TEST_METHOD(TestIsMeasured)
		{
			Qubit plus = QubitPlus();
			assert(plus.IsQubitMeasured() == false);
		}

		TEST_METHOD(TestSetMeasured)
		{
			Qubit plus = QubitPlus();
			plus.MeasureQubit();
			assert(plus.IsQubitMeasured() == true);
		}
	};
}