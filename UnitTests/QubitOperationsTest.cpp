#include "stdafx.h"
#include "CppUnitTest.h"
#include "Qubit.h"
#include "OperationExceptions.h"
#include "QRegister.h"
#include "IGate.h"
#include "GateFactory.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(QubitOperationsTests)
	{
	public:
		
		TEST_METHOD(TestEntangleTwoQubits)
		{
			Qubit q1 = QubitZero();
			Qubit q2 = QubitOne();
			Eigen::VectorXcd expectedQubitStates = Eigen::VectorXcd(4);
			expectedQubitStates(0) = std::complex<double>(0.0, 0.0);
			expectedQubitStates(1) = std::complex<double>(1.0, 0.0);
			expectedQubitStates(2) = std::complex<double>(0.0, 0.0);
			expectedQubitStates(3) = std::complex<double>(0.0, 0.0);
			Qubit expected = Qubit(expectedQubitStates);
			Qubit actual = q1*q2;
			assert(expected.GetQubitStates().isApprox(actual.GetQubitStates()) == true);
		}

		TEST_METHOD(TestEntangleMultipleQubits)
		{
			QRegister qubits=QRegister(3);
			qubits.push(QubitZero());
			qubits.push(QubitZero());
			qubits.push(QubitOne());
			Eigen::VectorXcd expectedQubitStates = Eigen::VectorXcd(8);
			expectedQubitStates(0) = std::complex<double>(0.0, 0.0);
			expectedQubitStates(1) = std::complex<double>(1.0, 0.0);
			expectedQubitStates(2) = std::complex<double>(0.0, 0.0);
			expectedQubitStates(3) = std::complex<double>(0.0, 0.0);
			expectedQubitStates(4) = std::complex<double>(0.0, 0.0);
			expectedQubitStates(5) = std::complex<double>(0.0, 0.0);
			expectedQubitStates(6) = std::complex<double>(0.0, 0.0);
			expectedQubitStates(7) = std::complex<double>(0.0, 0.0);
			Qubit expected = Qubit(expectedQubitStates);
			Qubit actual = qubits();
			assert(expected.GetQubitStates().isApprox(actual.GetQubitStates()) == true);
		}

		
	};
}




