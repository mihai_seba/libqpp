// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#ifdef __unix__         
#define LINUX
#elif defined(_WIN32) || defined(WIN32) 
#define WINDOWS

#endif
#ifdef WINDOWS
#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif // _WIN32


// Headers for CppUnitTest
#include <memory>
#include <mutex>
#include <iostream>
#include <complex>
#include <vector>
#include <exception>
#include <cmath>
#include <cstdlib>
#include <Eigen/Dense>
#include "CppUnitTest.h"

