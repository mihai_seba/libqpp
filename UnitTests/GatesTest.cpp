#include "stdafx.h"
#include "CppUnitTest.h"
#include "Qubit.h"
#include "OperationExceptions.h"
#include "QRegister.h"
#include "IGate.h"
#include "QRegisterOperations.h"
#include "GateFactory.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(GatesTest)
	{
	public:
		TEST_METHOD(CreateGate)
		{
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(EHadamardGate);
			assert(instance != nullptr);
		}

		TEST_METHOD(ApplyHadamardGate)
		{
			
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(EHadamardGate);
			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);

			regOps->FillWithPattern(targetRegister.get(), "000");
			regOps->FillWithPattern(expectedRegister.get(), "000");
			(*expectedRegister)[0] = QubitPlus();
			Qubit target = (*targetRegister.get())();
				
			instance->Apply(&target, { 0 }, 3);
			Logger::WriteMessage(target.to_string().c_str());
			Logger::WriteMessage((*expectedRegister.get())().to_string().c_str());
			assert(target == (*expectedRegister.get())());
		}

		TEST_METHOD(ApplyReverseHadamardGate)
		{
			
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(EHadamardGate);
			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);

			regOps->FillWithPattern(targetRegister.get(), "000");
			regOps->FillWithPattern(expectedRegister.get(), "000");
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, { 0 }, 3);
			instance->Apply(&target, { 0 }, 3);
			Logger::WriteMessage(target.to_string().c_str());
			Logger::WriteMessage((*expectedRegister.get())().to_string().c_str());
			//assert(target == QubitOperations::Entangle(*expectedRegister.get()));
		}


		TEST_METHOD(ApplyXGateQubitZero)
		{
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(EXGate);
			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);
			
			regOps->FillWithPattern(targetRegister.get(), "000");
			regOps->FillWithPattern(expectedRegister.get(), "100");
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, { 0 }, 3);
			assert(target == (*expectedRegister.get())());

		}

		TEST_METHOD(ApplyXGateQubitOne)
		{
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(EXGate);
			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);

			regOps->FillWithPattern(targetRegister.get(), "111");
			regOps->FillWithPattern(expectedRegister.get(), "110");
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, { 2 }, 3);
			assert(target == (*expectedRegister.get())());
		}

		TEST_METHOD(ApplyXGateTwoQubits)
		{
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(EXGate);
			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);

			regOps->FillWithPattern(targetRegister.get(), "111");
			regOps->FillWithPattern(expectedRegister.get(), "001");
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, { 0,1 }, 3);
			assert(target == (*expectedRegister.get())());
		}

		TEST_METHOD(ApplyYGateQubitZero)
		{

			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			Qubit q = Qubit(std::complex<double>(0.0, 0.0), std::complex<double>(0.0, 1.0));
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(EYGate);
			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);
			regOps->FillWithPattern(targetRegister.get(), "000");
			regOps->FillWithPattern(expectedRegister.get(), "000");
			(*expectedRegister)[0] = q;
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, { 0 }, 3);
			assert(target == (*expectedRegister.get())());

		}

		TEST_METHOD(ApplyYGateQubitOne)
		{

			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			Qubit q = Qubit(std::complex<double>(0.0, -1.0), std::complex<double>(0.0, 0.0));
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(EYGate);
			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);
			regOps->FillWithPattern(targetRegister.get(), "111");
			regOps->FillWithPattern(expectedRegister.get(), "111");
			(*expectedRegister)[0] = q;
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, { 0 }, 3);
			assert(target == (*expectedRegister.get())());
			

		}

		TEST_METHOD(ApplyCnotGateQubitOne)
		{
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(ECnotGate);
			
			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);
			regOps->FillWithPattern(targetRegister.get(), "111");
			regOps->FillWithPattern(expectedRegister.get(), "110");
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, 2, { 0, 1 }, 3);
			
			assert(target ==(*expectedRegister.get())());
			

		}
		TEST_METHOD(ApplyCnotGateQubitZero)
		{
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(ECnotGate);

			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);
			regOps->FillWithPattern(targetRegister.get(), "110");
			regOps->FillWithPattern(expectedRegister.get(), "111");
			Qubit target =(*targetRegister.get())();
			instance->Apply(&target, 2, { 0, 1 }, 3);

			assert(target == (*expectedRegister.get())());

		}

		TEST_METHOD(ApplyCnotGateAllQubitZero)
		{
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(ECnotGate);

			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);
			regOps->FillWithPattern(targetRegister.get(), "000");
			regOps->FillWithPattern(expectedRegister.get(), "000");
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, 2, { 0, 1 }, 3);

			assert(target == (*expectedRegister.get())());

		}

		TEST_METHOD(ApplyCnotGateFirstQubitZero)
		{
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(ECnotGate);

			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);
			regOps->FillWithPattern(targetRegister.get(), "010");
			regOps->FillWithPattern(expectedRegister.get(), "010");
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, 2, { 0, 1 }, 3);

			assert(target == (*expectedRegister.get())());

		}

		TEST_METHOD(ApplyZGateQubitZero)
		{
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			Qubit q = Qubit(std::complex<double>(1.0, 0.0), std::complex<double>(0.0, 0.0));
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(EZGate);
			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);
			regOps->FillWithPattern(targetRegister.get(), "000");
			regOps->FillWithPattern(expectedRegister.get(), "000");
			(*expectedRegister)[0] = q;
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, { 0 }, 3);
			assert(target == (*expectedRegister.get())());
			
		}

		TEST_METHOD(ApplyZGateQubitOne)
		{
			std::shared_ptr<QRegister> targetRegister = std::make_shared<QRegister>();
			std::shared_ptr<QRegister> expectedRegister = std::make_shared<QRegister>();
			QRegisterOperations *regOps = &QRegisterOperations::GetInstance();
			Qubit q = Qubit(std::complex<double>(0.0, 0.0), std::complex<double>(-1.0, 0.0));
			gGateFactory = GateFactory();
			instance = gGateFactory.CreateGate(EZGate);
			targetRegister->reserve_space(3);
			expectedRegister->reserve_space(3);
			regOps->FillWithPattern(targetRegister.get(), "111");
			regOps->FillWithPattern(expectedRegister.get(), "111");
			(*expectedRegister)[0] = q;
			Qubit target = (*targetRegister.get())();
			instance->Apply(&target, { 0 }, 3);
			assert(target == (*expectedRegister.get())());

		}

		

	private:
		std::shared_ptr<IGate> instance;
		
		GateFactory gGateFactory;
	};
}